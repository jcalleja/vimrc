set wildmode=list:longest
set autoindent

set tabstop=4
set shiftwidth=4

nnoremap <C-l> gt
nnoremap <C-h> gT
" nmap <C-l> gt
" nmap <C-h> gT

vmap <C-C> "+y
highlight WhitespaceEOL ctermbg=Grey guibg=Grey
match WhitespaceEOL /\s\+$/
nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

execute pathogen#infect()
syntax on
filetype plugin indent on

" syntax on
" colorscheme default
" colorscheme koehler
set term=xterm-256color
syntax enable
set background=dark
let g:solarized_termcolors=256
colorscheme solarized
" colorscheme af
" colorscheme Tomorrow-Night

" function! InsertTabWrapper()
" 	let col = col(".") - 1
" 	if !col || getline(".")[col - 1] !~ '\k'
" 		return "\<tab>"
" 	else
" 		return "\<c-n>"
" endfunction
"
" inoremap <tab> <c-r>=InsertTabWrapper()<cr>
" inoremap <s-tab> <c-p>

cnoreabbrev wrap set wrap
cnoreabbrev nowrap set nowrap

set laststatus=2

" set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P
set statusline=\                                      " Switch to this colorscheme's todo highlight group.
set statusline+=%#underlined#                         " Switch to this colorscheme's todo highlight group.
set statusline+=\ %f\                                 " Path to the file.
set statusline+=%*                                    " Set status line back to normal i.e. change back to using the statusline highlight group.
set statusline+=\ line:%l/%L\                         " Current line number / total line numbers.
set statusline+=col:%c\                               " Column number.
set statusline+=%p%%\                                 " Current line's percentage in file.
set statusline+=%y\                                   " Filetype of current file.
set statusline+=%#todo#                               " Change color for modified part. If file is modified, the [+] is shown using the current colorscheme's todo highlight group.
set statusline+=%m                                    " %m is to get [+] for modified file.
set statusline+=%*                                    " Set status line back to normal i.e. change back to using the statusline highlight group.

set runtimepath^=~/.vim/bundle/ctrlp.vim
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,target

" cnoreabbrev f CtrlP

set backspace=2 " make backspace work like most other apps

let mapleader=","

" let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:20,results:20'
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:30'
let g:ctrlp_working_path_mode = 0
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(class)$'
  \ }


hi TabLine      ctermfg=Black  ctermbg=Gray     cterm=NONE
hi TabLineFill  ctermfg=Black  ctermbg=Gray     cterm=NONE
hi TabLineSel   ctermfg=White  ctermbg=Black  cterm=NONE

let g:syntastic_mode_map = { 'mode': 'passive' }
cnoreabbrev check SyntasticCheck

" map <F5> yyp!!sh<CR><Esc>
" cnoreabbrev cp let $CLASSPATH

set shell=bash\ --login

" Map ✠ (U+2720) to <C-t> as <M-t> (i.e. alt + t) is mapped to ✠ in iTerm2.
" nnoremap ✠ <C-t>

" Map ï to <C-w> as <M-w> (i.e. alt + w) is mapped to ï in iTerm2.
" nnoremap ï <C-w>

" Toggle the file browser
" nnoremap ǰ :NERDTreeToggle<CR>
" nnoremap <F10> :NERDTreeToggle<CR>
"
nnoremap <space> :NERDTreeToggle<CR>
"
" Find the current file in the file browser
" nnoremap ǭ :NERDTreeFind<CR>
" nnoremap <F9> :NERDTreeFind<CR>
"
nnoremap <leader><space> :NERDTreeFind<CR>

noremap <leader>o <C-W>o


" nnoremap Ƿ <C-r>
" inoremap Ƿ <C-r>

" nnoremap <space> o<esc>kO<esc>j

" iTerm2 key bindings:
" alt + \, alt + z = ▄
" alt + s = ǯ
" alt + t = ✠
" alt + w = ï
" alt + 1 = ǰ
" alt + 2 = ǭ
" alt + r = Ƿ
"
" Might be useful:
" http://utf8-chartable.de/unicode-utf8-table.pl?start=256

" r ! source ~/envs/differ.env; echo $DIFFER_BLAH
" :let $PATH = '/foo:/bar'

" function! GetCurrentClasspath()
" 	return ! 

function! RefreshClasspath()
	! source $ENVFILE; echo "$CLASSPATH" > ~/.currentclasspath
	let $CLASSPATH = system("cat ~/.currentclasspath")
endfunction

" cnoreabbrev cj call CloseJavaDirInNerdTree()<CR>
" function! CloseJavaDirInNerdTree()
" " silent doesn't work...
" 	" silent /src\//;/java\/
" 	/<
" 	normal O<esc>
" 	/java\/
" 	normal o<esc>
" 	return 0
" 	" call nerdtree#closeTreeIfOpen()
" 	" call NERDTreeFocus()
" endfunction
" 
" cnoreabbrev oj call CloseJavaDirInNerdTree()<CR>
" function! OpenJavaDirInNerdTree()
" " silent doesn't work...
" 	" silent /src\//;/java\/
" 	/<-
" 	normal <C-p>
" 	
" 	normal o<esc>
" 	return 0
" 	" call nerdtree#closeTreeIfOpen()
" 	" call NERDTreeFocus()
" endfunction

" set hlsearch
" cnoreabbrev hl set hlsearch<cr>
" cnoreabbrev nohl nohlsearch<cr>
" nnoremap <leader>/ :noh<cr>
nnoremap <leader>/ :set hls! <cr>
set linebreak
nnoremap Y y$

" set mouse=nicr
set mouse=a

if !empty($PROJECT_VIMRC)
	source $PROJECT_VIMRC
endif

if !empty($NERDTREE_BOOKMARKS)
    if filereadable($NERDTREE_BOOKMARKS)
        let g:NERDTreeBookmarksFile = $NERDTREE_BOOKMARKS
    endif
endif

autocmd BufRead,BufNewFile *.tagx set filetype=xml
autocmd BufRead,BufNewFile *.jspx set filetype=xml

nnoremap <leader>cd :cd %:p:h <cr>              " change CWD (for all windows in Vim) to the directory containing %
nnoremap <leader>lcd :lcd %:p:h <cr>            " change CWD (only for the current window) to the directory containing %
nnoremap <leader>cdh :cd $PROJECT_HOME <cr>     " change CWD (for all windows in Vim) to the directory containing %
nnoremap <leader>lcdh :lcd $PROJECT_HOME <cr>    " change CWD (only for the current window) to the directory containing %

